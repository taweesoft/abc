/**
 * Created by taweesoft on 24/4/2558.
 */
var GreenBulletWeapon = Weapons.extend({
    ctor : function(){
        this._super();
        this.bulletNumber = 3;
        this.ammo = 2;
        this.initWithFile('res/GreenBulletWeapon.png');
    }
})