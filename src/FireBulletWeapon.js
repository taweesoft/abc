/**
 * Created by taweesoft on 22/4/2558.
 */
var FireBulletWeapon = Weapons.extend({
    ctor : function(){
        this._super();
        this.bulletNumber = 2;
        this.ammo = 5;
        this.initWithFile('res/FireBulletWeapon.png');
    }
})