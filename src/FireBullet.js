/**
 * Created by taweesoft on 20/4/2558.
 */
var FireBullet = Bullet.extend({
    ctor : function(blockList,enemyList){
        this._super(blockList,enemyList,2);
        this.initWithFile('res/FireBullet.png');
    }
})