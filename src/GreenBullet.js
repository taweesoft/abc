/**
 * Created by taweesoft on 24/4/2558.
 */
var GreenBullet = Bullet.extend({
    ctor : function(blockList,enemyList){
        this._super(blockList,enemyList,3);
        this.initWithFile('res/GreenBullet.png');
    }
})