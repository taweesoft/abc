/**
 * Created by taweesoft on 24/4/2558.
 */
var NukeBulletWeapon = Weapons.extend({
    ctor : function(){
        this._super();
        this.bulletNumber = 4;
        this.ammo = 1;
        this.initWithFile('res/NukeBulletWeapon.png');
    }
})