/**
 * Created by taweesoft on 25/3/2558.
 */
var Map = cc.Node.extend({
    ctor: function() {
        this._super();
        this.WIDTH = 20;
        this.HEIGHT = 13;
        this.MAP = [
            '####################',
            '##...#...#....#....#',
            '#....#...###..#.#..#',
            '#....#..........#..#',
            '#..####............#',
            '#........#...###...#',
            '#...#....#.........#',
            '#...###..#.........#',
            '#............####..#',
            '#..................#',
            '#....#...###....#..#',
            '#....#..........#..#',
            '####################'
        ];
        this.setAnchorPoint( cc.p( 0, 0 ) );
        this.blockList = [];
        for ( var r = 0; r < this.HEIGHT; r++ ) {
            for ( var c = 0; c < this.WIDTH; c++ ) {
                if ( this.MAP[ r ][ c ] == '#' ) {
                    var s = new Block();
                    //s.setAnchorPoint( cc.p( 0, 0 ) );
                    s.setPosition( cc.p( c * 40, (this.HEIGHT - r - 1) * 40 ) );
                    this.blockList.push(s);
                    this.addChild( s );
                }else if(this.MAP[r][c] == '$'){
                    var s = cc.Sprite.create('res/base_block.png');
                    //s.setAnchorPoint( cc.p( 0, 0 ) );
                    s.setPosition( cc.p( c * 40, (this.HEIGHT - r - 1) * 40 ) );
                    this.blockList.push(s);
                    this.addChild( s );
                }
            }
        }
        // ...  code for drawing the maze has be left out

    },
    isWall : function(blockX,blockY){

    }
})