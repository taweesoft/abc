/**
 * Created by taweesoft on 25/3/2558.
 */
var Bullet = cc.Sprite.extend({
    ctor : function(blockList,enemyList,damage) {
        this._super();
        this.damage = damage;
        console.log(damage);
        this.direction = Player.DIR.UP;
        this.blockList = blockList;
        this.enemyList = enemyList;
    },
    update : function(){
        var hitBlock = this.hitBlock();
        if(hitBlock==null){
            this.updatePosition();
        }else{
            console.log("Hit block : " + hitBlock);
        }

        var hitEnemy = this.hitEnemy();
        if(hitEnemy == null){
            this.updatePosition();
        }else{
            console.log("Hit enemy : " + hitEnemy);
            console.log(this.damage);
            hitEnemy.attacked(this.damage);
            cc.audioEngine.playEffect('res/sounds/explosion.mp3');
        }

    },

    updatePosition : function(){
        switch(this.direction){
            case Player.DIR.LEFT :
                this.setPosition(cc.p(this.x-5,this.y));
                this.position = Player.DIR.LEFT;
                this.setRotation(180);
                break;
            case Player.DIR.RIGHT :
                this.setPosition(cc.p(this.x+5,this.y));
                this.position = Player.DIR.RIGHT;
                break;
            case Player.DIR.DOWN :
                this.setPosition(cc.p(this.x,this.y-5));
                this.position = Player.DIR.DOWN;
                this.setRotation(90);
                break;
            case Player.DIR.UP :
                this.setPosition(cc.p(this.x,this.y+5));
                this.position = Player.DIR.UP;
                this.setRotation(270);
                break;
        }
    },

    setDirection : function(direction){
        this.direction = direction;
    },

    hitBlock : function(){
        return this.checkBulletIntercept(this.blockList);
    },

    hitEnemy : function(){
        return this.checkBulletIntercept(this.enemyList);
    },

    checkBulletIntercept : function(objectList) {
        for (var i = 0; i < objectList.length; i++) {
            var blockPosition = objectList[i].getPosition();
            var bulletPosition = this.getPosition();
            var x1 = bulletPosition.x - 5;
            var x2 = bulletPosition.x + 5;
            var x3 = blockPosition.x - 20;
            var x4 = blockPosition.x + 20;
            var y1 = bulletPosition.y + 5;
            var y2 = bulletPosition.y - 5;
            var y3 = blockPosition.y + 20;
            var y4 = blockPosition.y - 20;
            if (y2 <= y3 && x1 <= x4 && x2 >= x3 && y1 >= y4) {
                this.removeFromParent();
                return objectList[i];
            }
        }
        return null;
    }
})