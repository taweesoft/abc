/**
 * Created by taweesoft on 23/3/2558.
 */
var GameLayer = cc.LayerColor.extend({
    init : function(){
        var frameSize = cc.director.getWinSize();
        var centerPosition = cc.p(frameSize.width/2,frameSize.height/2);
        var bg = new cc.Sprite.create('res/bg.jpg');
        bg.setPosition(centerPosition);
        this.addChild(bg);
        state = GameLayer.STATE.START;
        this.weapons = [];
        this.map = new Map();
        this.map.setPosition(cc.p(20,40));
        this.player1 = this.createPlayer("Player1",cc.p(40,60),1);
        this.player2 = this.createPlayer("Player2",cc.p(720,440),2);
        this.map.addChild(this.player1);
        this.map.addChild(this.player2);
        this.player1Enemy = [];
        this.player1Enemy.push(this.player2);
        this.player2Enemy = [];
        this.player2Enemy.push(this.player1);
        this.player1.setEnemy(this.player1Enemy);
        this.player2.setEnemy(this.player2Enemy);
        this.player1.scheduleUpdate();
        this.player2.scheduleUpdate();
        this.addChild(this.map);
        this.scheduleUpdate();
        this.addKeyboardHandlers();
        this.start_state = new StartState();
        this.gameover_state = new GameoverState();
        this.addChild(this.start_state);
    },

    update : function(){
        if(state == GameLayer.STATE.PLAYING && (this.player1.isDead || this.player2.isDead )){
            state = GameLayer.STATE.STOP;
            this.addChild(this.gameover_state);
        }
        var random = parseInt(Math.random()*2000) + 1;
        if((random%17==0 || random % 29 == 0 || random % 37 ==0 || random % 41==0)  && this.weapons.length != 2){
            var correctPosition;
            do{
                correctPosition = cc.p((parseInt(Math.random()*15)+1)*40,(parseInt(Math.random()*10)+1)*40);
            }while(!this.checkCorrectPosition(correctPosition))
            var weapon;
            if(random%17==0)
                weapon = new FireBulletWeapon();
            if(random%29 == 0)
                weapon = new GreenBulletWeapon();
            if(random%37 == 0)
                weapon = new HeartItem();
            if(random%41 == 0)
                weapon = new NukeBulletWeapon();
            console.log(random);
            weapon.setPosition(correctPosition);
            this.weapons.push(weapon);
            this.map.addChild(weapon);
        }
    },

    checkCorrectPosition : function(correctPosition){
        for(var i =0;i<this.map.blockList.length;i++){
            var blockX = this.map.blockList[i].getPosition().x;
                   var blockY = this.map.blockList[i].getPosition().y;
            var correctX = correctPosition.x;
            var correctY = correctPosition.y;
            if(blockX == correctX && blockY == correctY)
                return false;
        }
        return true;
    },

    createPlayer : function(name,position,playerNumber){
        var player = new Player(this.map,name,playerNumber,this);
        player.setPosition(position);
        player.setBlockList(this.map.blockList);
        player.setWeaponsList(this.weapons);
        var hearts = this.createHeart(playerNumber);
        player.setHearts(hearts);
        var scoreLabel = this.createScoreLabel(player,playerNumber);
        player.setScoreLabel(scoreLabel);
        return player;
    },

    createScoreLabel : function(player,playerNumber){
        var scoreLabel = cc.LabelTTF.create(player.hearts.length+"",'Arial',40);
        var position;
        if( playerNumber == 1)
            position = new cc.Point(250,570);
        else if ( playerNumber == 2 )
            position = new cc.Point(750,570);
        scoreLabel.setPosition(position);
        this.addChild(scoreLabel);
        return scoreLabel;

    },

    createHeart : function(playerNumber){
        var hearts = [];
        for(var i=1;i<=5;i++){
            var heart = cc.Sprite.create('res/heart.png');
            if(playerNumber == 1)
                heart.setPosition(cc.p(i*40,570))
            else if(playerNumber == 2)
                heart.setPosition(cc.p((i*40)+500,570));
            this.addChild(heart);
            hearts.push(heart);
        }
        return hearts;
    },

    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },

    onKeyDown: function(keyCode, event) {
        if(state == GameLayer.STATE.PLAYING){
            switch (keyCode) {
                case cc.KEY.forwardslash :
                    if (!this.player1.isFire) {
                        this.player1.isFire = true;
                        this.player1.fire();
                    }
                    break;
                case cc.KEY.up :
                    this.player1.moveUp = true;
                    break;
                case cc.KEY.down :
                    this.player1.moveDown = true;
                    break;
                case cc.KEY.left :
                    this.player1.moveLeft = true;
                    break;
                case cc.KEY.right :
                    this.player1.moveRight = true;
                    break;
                case cc.KEY.a :
                    this.player2.moveLeft = true;
                    break;
                case cc.KEY.w :
                    this.player2.moveUp = true;
                    break;
                case cc.KEY.s :
                    this.player2.moveDown = true;
                    break;
                case cc.KEY.d :
                    this.player2.moveRight = true;
                    break;
                case cc.KEY.g :
                    if(!this.player2.isFire) {
                        this.player2.isFire = true;
                        this.player2.fire();
                    }
                    break;
            }
        }else if(state ==  GameLayer.STATE.START){
            if(keyCode == cc.KEY.enter){
                state = GameLayer.STATE.PLAYING;
                this.start_state.removeFromParent();
            }
        }else if(state == GameLayer.STATE.STOP){
            if(keyCode == cc.KEY.enter){
                state = GameLayer.STATE.START;
                cc.game.restart();
            }
        }

    },

    onKeyUp: function(keyCode,event){
        switch (keyCode) {
            case cc.KEY.forwardslash :
                this.player1.isFire = false;
                break;
            case cc.KEY.up :
                this.player1.moveUp = false;
                break;
            case cc.KEY.down :
                this.player1.moveDown = false;
                break;
            case cc.KEY.left :
                this.player1.moveLeft = false;
                break;
            case cc.KEY.right :
                this.player1.moveRight = false;
                break;
            case cc.KEY.a :
                this.player2.moveLeft = false;
                break;
            case cc.KEY.w :
                this.player2.moveUp = false;
                break;
            case cc.KEY.s :
                this.player2.moveDown = false;
                break;
            case cc.KEY.d :
                this.player2.moveRight = false;
                break;
            case cc.KEY.g :
                this.player2.isFire = false;
                break;
        }
    },

    gameOver : function(){

    }

});
var StartScene = cc.Scene.extend({
    onEnter : function(){
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild(layer);
    }
});

GameLayer.STATE = {
    START : 1 ,
    PLAYING : 2,
    STOP : 3
}

var state = GameLayer.STATE.START;