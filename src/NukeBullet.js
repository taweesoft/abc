/**
 * Created by taweesoft on 24/4/2558.
 */
var NukeBullet = Bullet.extend({
    ctor : function(blockList,enemyList){
        this._super(blockList,enemyList,5);
        this.initWithFile('res/NukeBullet.png');
    }
})