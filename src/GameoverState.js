/**
 * Created by taweesoft on 1/5/2558.
 */
var GameoverState = cc.Sprite.extend({

    ctor : function(){
        this._super();
        this.initWithFile('res/gameover_state.png');
        this.setPosition(new cc.Point(screenWidth/2, screenHeight/2));
    }

})