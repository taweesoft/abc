/**
 * Created by taweesoft on 22/4/2558.
 */
var NormalBulletWeapon = Weapons.extend({
    ctor : function(){
        this._super();
        this.bulletNumber = 1;
        this.initWithFile('res/bullet.png');
    }
})