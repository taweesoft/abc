var res = {
    BackGround_jpg : "res/bg.jpg",
    CloseNormal_png : "res/CloseNormal.png",
    CloseSelected_png : "res/CloseSelected.png",
    ShotSound : "res/sounds/shot.mp3",
    ExplosionSound : "res/sounds/explosion.mp3",
    PickItem : "res/sounds/pick.mp3",
    HeartPickSound  : "res/sounds/ping.mp3",
    WinSound : "res/sounds/win.mp3"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}