/**
 * Created by taweesoft on 23/3/2558.
 */
var Player = cc.Sprite.extend({

   ctor : function(map,name,playerNumber,gameLayer){
       this._super();
       this.position = Player.DIR.UP;
       this.gameLayer = gameLayer;
       switch(playerNumber){
           case 1 :
               this.initWithFile('res/tank1.png');
               break;
           case 2:
               this.initWithFile('res/tank2.png');
               break;
       }
       this.map = map;
       this.name = name;
       this.moveUnit = 3;
       this.isFire = false;
       this.blockList = null;
       this.moveUp = false;
       this.moveDown = false;
       this.moveLeft = false;
       this.moveRight = false;
       this.currentBullet = 1;
       this.playerNumber = playerNumber;
       this.ammo = -1;
       this.isDead = false;
   },

    update : function(){
        this.checkPicksWeapon();
        if(this.moveUp && this.canMoveNext()){
            this.setPosition(cc.p(this.x, this.y + this.moveUnit));
            this.position = Player.DIR.UP;
            this.setRotation(0);
        }
        if(this.moveDown && this.canMoveNext()){
            this.setPosition(cc.p(this.x, this.y - this.moveUnit));
            this.position = Player.DIR.DOWN;
            this.setRotation(180);
        }
        if(this.moveLeft & this.canMoveNext()){
            this.setPosition(cc.p(this.x - this.moveUnit, this.y));
            this.position = Player.DIR.LEFT;
            this.setRotation(270);
        }
        if(this.moveRight && this.canMoveNext()){
            this.setPosition(cc.p(this.x + this.moveUnit, this.y));
            this.position = Player.DIR.RIGHT;
            this.setRotation(90);
        }
    },

    fire : function() {
        if(this.isFire){
            cc.audioEngine.playEffect('res/sounds/shot.mp3');
            var bullet;
            switch(this.currentBullet){
                case 1 :
                    bullet = new NormalBullet(this.blockList,this.enemyList);
                    break;
                case 2 :
                    if(this.ammo != 0){
                        bullet = new FireBullet(this.blockList,this.enemyList);
                        this.ammo--;
                    }else{
                        this.currentBullet = 1;
                        bullet = new NormalBullet(this.blockList,this.enemyList);
                    }
                    break;
                case 3 :
                    if(this.ammo != 0){
                        bullet = new GreenBullet(this.blockList,this.enemyList);
                        this.ammo--;
                    }else{
                        this.currentBullet = 1;
                        bullet = new NormalBullet(this.blockList,this.enemyList);
                    }
                    break;
                case 4 :
                    if(this.ammo != 0){
                        bullet = new NukeBullet(this.blockList,this.enemyList);
                        this.ammo--;
                    }else{
                        this.currentBullet = 1;
                        bullet = new NormalBullet(this.blockList,this.enemyList);
                    }
                    break;
            }
            var position = this.position;
            var pos = this.getPosition();
            bullet.setPosition(cc.p(pos.x,pos.y));
            bullet.setDirection(position);
            this.map.addChild(bullet);
            bullet.scheduleUpdate();
        }

    },

    setDirection : function(direction){
        this.position = direction;
    },

    canMoveNext : function(){
        for(var i=0;i<this.blockList.length;i++){
            var blockPosition = this.blockList[i].getPosition();
            var playerPosition = this.getPosition();
            var x1 = playerPosition.x-14;
            var x2 = playerPosition.x+14;
            var x3 = blockPosition.x-20;
            var x4 = blockPosition.x+20;
            var y1 = playerPosition.y+14;
            var y2 = playerPosition.y-14;
            var y3 = blockPosition.y+20;
            var y4 = blockPosition.y-20;
            if(this.moveUp && ((x2 > x3 && x2 < x4) || (x1 > x3 && x1 < x4)) && y1+3 >= y4 && y1+3 <= y3)
                return false;
            if(this.moveDown && ((x2 > x3 && x2 < x4) || (x1 > x3 && x1 < x4)) && y2-3 <= y3 && y2-3 >= y4)
                return false;
            if(this.moveLeft && ((y2 < y3 && y2 > y4) || (y1 < y3 && y1 > y4)) && x1-3 <= x4 && x1-3 >= x3)
                return false;
            if(this.moveRight && ((y2 < y3 && y2 > y4) || (y1 < y3 && y1 > y4)) && x2+3 >= x3 && x2+3 <= x4)
                return false;
        }
        return true;
    },

    setBlockList : function(blockList){
        this.blockList = blockList;
    },

    setEnemy : function(enemyList){
        this.enemyList = enemyList;
    },

    setScoreLabel : function(scoreLabel){
        this.scoreLabel = scoreLabel;
    },

    attacked : function(damage){
        for(var i=0;i<damage;i++){
            var heart = this.hearts.pop();
            if(heart == null) break;
            heart.removeFromParent();
        }
        this.scoreLabel.setString(this.hearts.length);
        if(this.hearts.length ==0 ){
            cc.audioEngine.playEffect('res/sounds/win.mp3');
            this.removeFromParent();
            this.isDead = true;
        }
    },

    setHearts : function(hearts){
        this.hearts = hearts;
    },

    setWeaponsList : function(weapons) {
        this.weaponsList = weapons;
    },

    checkPicksWeapon : function(){
        for (var i = 0; i < this.weaponsList.length; i++) {
            var blockPosition = this.weaponsList[i].getPosition();
            var bulletPosition = this.getPosition();
            var x1 = bulletPosition.x - 20;
            var x2 = bulletPosition.x + 20;
            var x3 = blockPosition.x - 15;
            var x4 = blockPosition.x + 15;
            var y1 = bulletPosition.y + 20;
            var y2 = bulletPosition.y - 20;
            var y3 = blockPosition.y + 15;
            var y4 = blockPosition.y - 15;
            if (y2 <= y3 && x1 <= x4 && x2 >= x3 && y1 >= y4) {
                var weapon = this.weaponsList[i];
                if(weapon.bulletNumber==0){
                    if(this.hearts.length!=5){
                        cc.audioEngine.playEffect('res/sounds/ping.mp3');
                        this.gotHeart();
                        this.weaponsList[i].removeFromParent();
                        this.weaponsList.splice(i,1);
                        return null;
                    }
                }else{
                    cc.audioEngine.playEffect('res/sounds/pick.mp3');
                    this.ammo = weapon.ammo;
                    this.currentBullet = weapon.bulletNumber;
                    weapon.removeFromParent();
                    this.weaponsList.splice(i,1);
                    return weapon;
                }

            }
        }
        return null;
    },

    gotHeart : function(){
            var heart = cc.Sprite.create('res/heart.png');
            if(this.playerNumber == 1)
                heart.setPosition(cc.p(((this.hearts.length+1)*40),570))
            else if(this.playerNumber == 2)
                heart.setPosition(cc.p(((this.hearts.length+1)*40)+500,570));
            this.gameLayer.addChild(heart);
            this.hearts.push(heart);
            this.scoreLabel.setString(this.hearts.length);
    }


});

Player.LOOKDIR = {
    FORWARD : 1,
    BACKWARD : 2
}
Player.DIR = {
    LEFT : 37,
    UP : 38,
    RIGHT : 39,
    DOWN : 40
}
Player.ACCELERATION = -1;
Player.STARTING_VELOCITY = 15;
Player.JUMPING_VELOCITY = 15;