/**
 * Created by taweesoft on 29/4/2558.
 */
var StartState = cc.Sprite.extend({
    ctor : function(){
        this._super();
        this.initWithFile('res/start_state.png');
        this.setPosition( new cc.Point(screenWidth/2,screenHeight/2));
    }
})