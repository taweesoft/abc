/**
 * Created by taweesoft on 22/4/2558.
 */
var NormalBullet = Bullet.extend({

    ctor : function(blockList,enemyList){
        this._super(blockList,enemyList,1);
        this.initWithFile('res/bullet.png');
    }
})